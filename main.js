const tabs = document.querySelector(".tabs")

tabs.addEventListener('click', function (event) {
    if ( event.target.classList.contains('tabs-title') ) {
        const currentTabTitle = event.target;
        const currentTabTitleTarget = currentTabTitle.getAttribute('data-tab');
        console.log('currentTabTitleTarget', currentTabTitleTarget)

        const targetTabsContentItem = document.getElementById(currentTabTitleTarget);
        const tabsTitles = document.querySelectorAll('.tabs-title');
        const tabsContentItems = document.querySelectorAll('.tabs-content-item');

        tabsTitles.forEach(function (item) {
            item.classList.remove('active');
        });

        tabsContentItems.forEach(function (item) {
            item.classList.remove('active');
        })

        currentTabTitle.classList.add('active');
        targetTabsContentItem.classList.add('active');

    }
})
